This is the repository for my personal website. 

# How I did it

I wrote a little step-by-step guide because maybe you, or even a future version of me, wants to know how I did it.

1. Install the `{markdown}`, `{rmarkdown}` and `{blogdown}` packages;

2. Make a new directory and create a project inside it with `blogdown::new_site()`;

3. Create a .gitignore file;

    ```
    .Rproj.user
    .Rhistory
    .RData
    .Ruserdata
    ```

4. Edit the website as you wish, check the evolution using `blogdown::render_site()`;

5. Create the *.gitlab-ci.yml* file;

    ```
    image: rocker/verse:4.2.0
    
    .blogdown:
      stage: deploy
      before_script:
        - apt update
        - apt upgrade -y
        - apt remove hugo -y
      script:
        - Rscript -e "install.packages('blogdown')"
        - Rscript -e "blogdown::install_hugo(version = '0.98.0')"
        - Rscript -e "blogdown::build_site()"
      artifacts:
        paths:
          - public
    
    pages:
      extends: .blogdown
      only:
        - main
    
    mr-review:
      extends: .blogdown
      after_script:
        - echo "ENVIRONMENT_URL=https://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html" >> deploy.env
      artifacts:
        reports:
          dotenv: deploy.env
      environment:
        name: review/$CI_COMMIT_REF_NAME
        url: $ENVIRONMENT_URL
      only:
        - merge_requests
    ```

6. Domain settings can be found [here](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/) and namecheap domain settings can be found [here](https://www.namecheap.com/support/knowledgebase/article.aspx/10446/2208/how-do-i-link-my-domain-to-gitlab-pages/).
